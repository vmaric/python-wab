import http.server as srv 
import urllib.request as req
import urllib.parse as parse
import importlib
import os
import ctrl.appcontroller

os.chdir("web-ex04/mymvc") 
 
class MyHandler(srv.SimpleHTTPRequestHandler):
    def do_GET(self):
        parts = parse.urlparse(self.path) 
        self.params = dict(parse.parse_qsl(parts.query))
        if "ctrl" in self.params:
            ctrlname = self.params["ctrl"] + "controller"
            mod = importlib.import_module("ctrl."+ctrlname,"ctrl")
            ctrlclass = getattr(mod,ctrlname) 
        else:  
            ctrlclass = ctrl.appcontroller.appcontroller
        c = ctrlclass(self)
        self.wfile.write("HTTP/1.1 200 Ok\r\n\r\n".encode("utf-8")) 
        if "mtd" in self.params:
            mtd = getattr(c,self.params["mtd"])
            mtd()
        else:
            c.index()
s = srv.HTTPServer(("localhost",8085),MyHandler)
s.serve_forever()