class controllerbase:
    def __init__(self,request):
        self.request = request 
    def index(self):
        self.loadView("index")
    def loadView(self,view,params={}):
        r = open(f"view/{view}.html")
        content = r.read()
        for k in params:
            content = content.replace("{{"+k+"}}",str(params[k]))
        self.request.wfile.write(content.encode("utf-8"))
        r.close() 

