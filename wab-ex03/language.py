import http.server
import http.cookies
import urllib.parse as parse 

languages = {
    "en":{"greet":"Good day","content":"Choose language"},
    "sr":{"greet":"Dobar dan","content":"Odaberi jezik"},
    "de":{"greet":"Schönen Tag","content":"Wähle die Sprache"}
}

class Handler(http.server.SimpleHTTPRequestHandler): 
    def do_GET(self): 
        params = dict(parse.parse_qsl(parse.urlparse(self.path).query)) 
        self.send_response_only(200) 
        self.flush_headers()
        ck = http.cookies.SimpleCookie() 
        language = "en"
        if "lang" in params and params["lang"] in languages: 
            ck["lang"] = params["lang"]  
            self.wfile.write((ck.output()+"\r\n").encode("utf-8")) 
            language = params["lang"]
        else:
            ck.load(self.headers["Cookie"])   
            if "lang" in ck and ck["lang"].value in languages:
                language = ck["lang"].value  
        self.send_header("Content-type","text/html; charset=UTF-8")
        self.end_headers()  
        self.wfile.write(languages[language]["greet"].encode()) 
        self.wfile.write(b"<br>") 
        self.wfile.write(languages[language]["content"].encode())
        self.wfile.write(b":<br>") 
        for k in languages:
            self.wfile.write(f"<a href='?lang={k}'>{k}</a> ".encode()) 

http.server.HTTPServer(("localhost",8000),Handler).serve_forever()