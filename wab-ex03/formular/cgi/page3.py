#!/usr/bin/env python3
import cgi
params = cgi.FieldStorage()
out = f"""
<form method="post" action="/cgi/review.py">
    <input type="hidden" name="firstname" value="{params.getvalue('firstname')}">
    <input type="hidden" name="lastname" value="{params.getvalue('lastname')}"> 
    <input type="hidden" name="streetnumber" value="{params.getvalue('streetnumber')}">
    <input type="hidden" name="streetname" value="{params.getvalue('streetname')}"> 
    Profession: <input type="text" name="profession" value="{params.getvalue('profession')}"><br>
    <input onclick="this.form.action='/cgi/page2.py'" type="submit" value="Next page" />
    <input type="submit" value="Send Data" />
</form>""" 
print("Content-Type: text/html")
print(out)