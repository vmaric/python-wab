#!/usr/bin/env python3
import cgi
params = cgi.FieldStorage()
out = f"""
<form method="post" action="/cgi/page2.py"> 
    <input type="hidden" name="streetnumber" value="{params.getvalue('streetnumber')}">
    <input type="hidden" name="streetname" value="{params.getvalue('streetname')}"> 
    <input type="hidden" name="profession" value="{params.getvalue('profession')}">
    First name: <input type="text" name="firstname" value="{params.getvalue('firstname')}"><br>
    Last name: <input type="text" name="lastname" value="{params.getvalue('lastname')}"><br>
    <input type="submit" value="Next page" />
</form>""" 
print("Content-Type: text/html")
print(out)