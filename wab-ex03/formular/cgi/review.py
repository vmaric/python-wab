#!/usr/bin/env python3
import cgi
params = cgi.FieldStorage() 
out = f"""
<strong>Your data</strong><br>
First name: {params.getvalue("firstname")}<br>
Last name: {params.getvalue("lastname")}<br>
Address: {params.getvalue("streetname")} {params.getvalue("streetnumber")}<br>
Profession: {params.getvalue("profession")}<br>
""" 
print("Content-Type: text/html")
print(out)