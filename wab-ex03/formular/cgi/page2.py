#!/usr/bin/env python3
import cgi
params = cgi.FieldStorage()
out = f"""
<form method="post" action="/cgi/page3.py">
    <input type="hidden" name="firstname" value="{params.getvalue('firstname')}">
    <input type="hidden" name="lastname" value="{params.getvalue('lastname')}"> 
    <input type="hidden" name="profession" value="{params.getvalue('profession')}">
    Street name: <input type="text" name="streetname" value="{params.getvalue('streetname')}"><br>
    Street number: <input type="text" name="streetnumber" value="{params.getvalue('streetnumber')}"><br>
    <input onclick="this.form.action='/cgi/page1.py'" type="submit" value="Prev page" />
    <input type="submit" value="Next page" />
</form>""" 
print("Content-Type: text/html")
print(out)