import http.server
import http.cookies as cookie
import uuid
import os
import model
import urllib.parse as parse 

REDIRECT_STATUS = 302

os.chdir("wab-ex03/pick")

heroes = {
    "akali":"akali.png",
    "amumu":"amumu.png",
    "cho":"cho.png",
    "fiddle":"fiddle.png",
    "kata":"kata.png",
    "panth":"panth.png",
    "teemo":"teemo.png",
    "vayne":"vayne.png"
}

def generate_heroes_list():
    output = "<div style='width:250px; margin: auto; text-align: center; '>"
    selected_heroes = [Handler.sessions[x]["hero"] for x in Handler.sessions if "hero" in Handler.sessions[x]]  
    for h in heroes: 
        hero = heroes[h] 
        is_selected = h in selected_heroes
        op = 'opacity: 0.2;' if is_selected else ''
        output += f"<a border=0 style='text-decoration: none' href='/pick?h={h}'><img style='margin:2px;{op}' src='images/{hero}'/></a>"
    output += "</div>" 
    return output

def generate_controls():
    return "<div onclick='window.location=\"/logout\"' style='text-align:center'><button>Logout</button></div>"

class Handler(http.server.SimpleHTTPRequestHandler):
    sessions = {}  
    session = None
    @staticmethod
    def start_session(req):
        ck = cookie.SimpleCookie()
        session_key = None
        try:
            hdr = req.headers["Cookie"]
            ck.load(hdr) 
            session_key = ck.get("pysession",None)
        except:
            pass
        if session_key:
            if not session_key.value in Handler.sessions:
                Handler.sessions[session_key.value] = {}
            req.session = session_key.value
        else:
            session_key = uuid.uuid1().hex
            Handler.sessions[session_key] = {}
            req.session = session_key
            ck["pysession"] = session_key 
            return ck.output()
    
    def do_GET(self): 
        if self.path.startswith('/pick'):
            Handler.start_session(self) 
            if "name" not in Handler.sessions[self.session]:  
                self.send_response(REDIRECT_STATUS)
                self.send_header("location","/")
                self.end_headers() 
                return
            params = dict(parse.parse_qsl(parse.urlparse(self.path).query))
            if "h" in params and params["h"] in heroes:
                if [h for h in Handler.sessions if "hero" in Handler.sessions[h] and Handler.sessions[h]["hero"] == params["h"]]:
                    self.send_response(REDIRECT_STATUS)
                    self.send_header("location","/heroes")
                    self.end_headers() 
                    return 
                else:
                    Handler.sessions[self.session]["hero"] = params["h"]
                    self.send_response(REDIRECT_STATUS)
                    self.send_header("location","/heroes")
                    self.end_headers()
            else:
                self.send_response(REDIRECT_STATUS)
                self.send_header("location","/heroes")
                self.end_headers() 
        elif self.path == '/logout': 
            Handler.start_session(self)
            del Handler.sessions[self.session] 
            self.send_response(REDIRECT_STATUS) 
            self.send_header("location","/") 
        elif self.path == '/heroes': 
            cookie = Handler.start_session(self) 
            if "name" not in Handler.sessions[self.session]: 
                self.send_response(REDIRECT_STATUS) 
                self.send_header("location","/") 
                self.end_headers()
            else: 
                self.send_response(200) 
                self.send_header("content-type","text/html")   
                self.end_headers()
                username = Handler.sessions[self.session]["name"]
                self.wfile.write(f"<h2 style='text-align: center;'>Your pick {username}</h2>".encode())
                self.wfile.write(generate_heroes_list().encode())
                self.wfile.write(generate_controls().encode())
        else:
            return super().do_GET()
    def do_POST(self):
        ln = int(self.headers["Content-length"]) 
        data = dict(parse.parse_qsl(self.rfile.read(ln).decode()))
        if "username" in data and "password" in data:
            if model.login(data["username"],data["password"]):
                cookie = Handler.start_session(self)
                Handler.sessions[self.session] = {"name":data["username"]}
                self.send_response(REDIRECT_STATUS)
                if cookie:
                    self.wfile.write((cookie+"\r\n").encode()) 
                self.send_header("location","/heroes")
            else:
                self.send_response(REDIRECT_STATUS)
                self.send_header("location","/")
        else:
            self.send_response(REDIRECT_STATUS)
            self.send_header("location","/") 
        self.end_headers()
        
                
        

http.server.HTTPServer(("localhost",8005),Handler).serve_forever()