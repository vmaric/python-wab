import http.server as server
import http.cookies as cookies
import uuid
import random
 
session_store = {}

class AppHandler(server.SimpleHTTPRequestHandler):  
    def do_GET(self): 
        self.send_response(200)  
        self.send_header("Connection","Close") 
        ck = cookies.SimpleCookie()  
        if "Cookie" in self.headers: 
            ck.load(self.headers["Cookie"])    
        your_number = -1 

        if "SESSION_KEY" in ck and ck["SESSION_KEY"].value in session_store:
            session_key = ck["SESSION_KEY"].value
            your_number = session_store[session_key] 
            self.end_headers() 
        else:
            self.flush_headers()
            session_key = uuid.uuid4().hex
            your_number = random.randint(0,100)
            session_store[session_key] = your_number
            ck = cookies.SimpleCookie() 
            ck["SESSION_KEY"] = session_key
            self.wfile.write(ck.output().encode("utf-8"))   
            self.wfile.write(b"\r\n\r\n")   
        
        self.wfile.write(f"Your number: {your_number}".encode("utf-8"))  
 
ss = server.HTTPServer(("0.0.0.0",8005),AppHandler)
ss.serve_forever()


