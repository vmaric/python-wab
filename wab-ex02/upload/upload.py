import http.server as server
import urllib.parse as parse
import io
import os

class UploadHandler(server.SimpleHTTPRequestHandler):  
    def do_POST(self):  
        l = int(self.headers["Content-Length"])
        boundary = self.headers["Content-Type"].split("=")[1]
        contentType = "text/html"
        while True:
            line = self.rfile.readline()
            l -= len(line)
            line = line.decode("utf-8")
            if 'Content-Type:' in line:
                contentType = line.split("Content-Type:")[1].strip()
            if line == '\r\n':
                break 
        self.wfile.write(f"HTTP/1.1 200 OK\r\nContent-Type: {contentType}\r\nConnection:close\r\n\r\n".encode("utf-8"))  
        while l > 0:
            line = self.rfile.readline() 
            l -= len(line) 
            self.wfile.write(line)

ss = server.HTTPServer(("0.0.0.0",8005),UploadHandler)
ss.serve_forever()