import http.server as server 
import importlib 

class AppHandler(server.SimpleHTTPRequestHandler):
    def do_GET(self):  
        if ".jpg" in self.path or ".png" in self.path:
            return super().do_GET()
        query = self.path.split("?")
        params = {}
        if len(query)>1:
            query = query[1] 
            p1 = query.split("&")
            for param in p1:
                p2 = param.split("=")
                if len(p2)>1:
                    params[p2[0]] = p2[1]  
        res = 'No params or params not valid'
        modpage = "index"
        if 'page' in params:
            modpage = params['page'] 
        try: 
            page = importlib.import_module(f"mod.{modpage}")
            res = page.render(params)
        except RuntimeError as err: 
            print(err) 
        self.wfile.write(f"HTTP/1.1 200 OK\r\nContent-Type:text/html\r\nConnection:close\r\n\r\n".encode("utf-8")) 
        self.wfile.write(res.encode("utf-8"))

ss = server.HTTPServer(("0.0.0.0",8005),AppHandler)
ss.serve_forever()