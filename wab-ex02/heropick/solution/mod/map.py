import model
def render(params):
    res = "<img width=800 src='images/map.png' style='opacity:0.6' />"
    if "hero" in params:
        for hero in model.heroes: 
            if hero["id"] == params["hero"]:
                res += f"<img style='position:absolute;left:{hero['pos'][0]}px;top:{hero['pos'][1]}px;' width=50 src='images/{hero['image']}' />"
                res += f"<h1 style='color:blue;position:absolute;top:5px;left:5px;width:800px;text-align:center;'>Your role: <span style='color:red;'>{hero['role']}</span></h1>"
    return res