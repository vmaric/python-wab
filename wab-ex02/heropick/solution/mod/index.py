import model
def render(params):
    res = ""
    for hero in model.heroes:
        res += f"<a href='?page=map&hero={hero['id']}'><img border=0 style='margin:2px;' width=50 src='images/{hero['image']}' /></a>"
    return res