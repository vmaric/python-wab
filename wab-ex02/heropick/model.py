heroes = [
    {"id":"1","name":"Akali","image":"akali.png","pos":[300,265],"role":"Mid"},
    {"id":"12","name":"Amumu","image":"amumu.png","pos":[170,250],"role":"Jungle"},
    {"id":"5","name":"Katarina","image":"kata.png","pos":[300,265],"role":"Mid"},
    {"id":"8","name":"Vayne","image":"vayne.png","pos":[634,452],"role":"ADC"},
    {"id":"25","name":"Panth","image":"panth.png","pos":[80,80],"role":"Top"},
    {"id":"31","name":"Cho","image":"cho.png","pos":[80,80],"role":"Top"},
    {"id":"32","name":"Fiddle","image":"fiddle.png","pos":[634,452],"role":"Support"},
    {"id":"2","name":"Teemo","image":"teemo.png","pos":[900,10],"role":"Outside the game!"}
]