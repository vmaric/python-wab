import http.server as server
import urllib.parse as parse

class AppHandler(server.SimpleHTTPRequestHandler):  

    def validate(self,params):
        if "tb_name" not in params or not params["tb_name"]:
            return "Username is not valid"
        #add more validation
        return ""

    def do_POST(self): 
        len = int(self.headers["Content-Length"])
        data = self.rfile.read(len).decode("utf-8")
        params = dict(parse.parse_qsl(data))
        err = self.validate(params)

        self.wfile.write(b"HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection:close\r\n\r\n") 
        
        if not err:
            username = params["tb_name"]
            email = params["tb_email"]
            gender = params["rb_gender"]
            message = params["ta_message"]
            above_13 = "cb_age" in params

            userdata = f"Username: {username}<br>Email: {email}<br>Gender: {gender}<br>Message: {message}<br>Above 13: {above_13}"

            self.wfile.write(f"User data ok<hr>{userdata}".encode("utf-8")) 
        else:
            self.wfile.write(f"{err}<hr>".encode("utf-8")) 

        
        

ss = server.HTTPServer(("0.0.0.0",8005),AppHandler)
ss.serve_forever()