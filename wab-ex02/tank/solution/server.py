import http.server as server 

model = {
    "tiger":{"name":"Tiger I","link":"https://en.wikipedia.org/wiki/Tiger_I","picture":"tiger1.jpg"},
    "panther":{"name":"Panther","link":"https://en.wikipedia.org/wiki/Panther_tank","picture":"panther.jpg"},
    "abrams":{"name":"M1 Abrams","link":"https://en.wikipedia.org/wiki/M1_Abrams","picture":"abrams.jpg"}
}

class TankHandler(server.SimpleHTTPRequestHandler):
    def do_GET(self):  
        if ".jpg" in self.path or ".png" in self.path:
            return super().do_GET()  
        query = self.path.split("?")
        params = {}
        if len(query)>1:
            query = query[1] 
            p1 = query.split("&")
            for param in p1:
                p2 = param.split("=")
                if len(p2)>1:
                    params[p2[0]] = p2[1] 
        res = ''
        if 'tank' in params:
            if params['tank'] in model:
                tank = model[params['tank']]
                res = f"""<div style='text-align:center;width:200px;height:250px;border:1px solid gray;padding:5px;'>
                        <h2 style='margin:10px;'>{tank['name']}</h2>
                        <img src='{tank['picture']}' width=200 />
                        <div>
                            <a href='{tank['link']}'>Read more</a>
                        </div>
                    </div>"""
            else:
                res = "Sorry, tank not found"

        self.wfile.write(f"HTTP/1.1 200 OK\r\nConnection:close\r\n\r\n".encode("utf-8")) 
        self.wfile.write("<a href='?tank=abrams'>M1 Abrams</a> <a href='?tank=tiger'>Tiger</a> <a href='?tank=panther'>Panther</a>".encode("utf-8"))
        self.wfile.write(res.encode("utf-8")) 

ss = server.HTTPServer(("0.0.0.0",8005),TankHandler)
ss.serve_forever()