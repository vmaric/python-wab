import os
import http.server 
os.chdir("wab-ex02/imagegallery/www") 
class Handler(http.server.SimpleHTTPRequestHandler): 
    def do_GET(self):
        if not self.path.lstrip("/"):
            self.send_response(200)
            self.send_header("Content-type","text/html")
            self.end_headers()
            for image in os.listdir("images"):
                self.wfile.write(f"<div style='border:1px solid red;padding:4px;margin:4px;float:left;'><img src='images/{image}' width=200 /></div>".encode())
        else:
            return super().do_GET()
    def do_POST(self): 
        total_len = int(self.headers["Content-Length"])
        boundary = self.headers["Content-Type"].split("boundary=")[1]  
        filename = ""
        line = self.rfile.readline()
        while line.strip():
            line = line.decode().strip()
            if "Content-Disposition" in line:
                filename = line.split('filename="')[1].rstrip('"')
            line = self.rfile.readline()
            print(line) 
        fl = open(f"images/{filename}","wb")
        line = self.rfile.readline() 
        while line:
            try:
                if boundary in line.decode().strip():
                    break
            except:
                pass
            fl.write(line)
            line = self.rfile.readline() 
        fl.close()
        self.send_response(303)
        self.send_header("Location","/")
        self.end_headers()

server = http.server.HTTPServer(("localhost",8000),Handler) 
server.serve_forever()
