import http.server as server
import datetime
import urllib.parse

class HelloHandler(server.SimpleHTTPRequestHandler):
    def do_GET(self):   

        parts = urllib.parse.urlparse(self.path)
        params = urllib.parse.parse_qs(parts.query)
        name = params['name'][0]
        
        self.wfile.write(f"HTTP/1.1 200 OK\r\nConnection:close\r\n\r\n".encode("utf-8")) 
        self.wfile.write(f"Hello {name}".encode("utf-8"))

ss = server.HTTPServer(("0.0.0.0",8005),HelloHandler)
ss.serve_forever()