import model
import os
import http.server as server
import urllib.parse as parser
 
class UploadHandler(server.SimpleHTTPRequestHandler):  
    def draw_manufacturers_select(self,selected=""):
        out = "<select name='manufacturer'>"
        for k in model.cars:
            out += "<option "
            if selected == k:
                out += "selected"
            out += f">{k}</option>"
        out += "</select>"
        out += "<input type='submit' name='man_sel' value='Show models' />"
        return out

    def draw_model_select(self,mod):
        out = "<select multiple name='models'>" 
        for mod in model.cars[mod]:
            out += f"<option>{mod}</option>"
        out += "</select>"
        out += "<input type='submit' name='mod_sel' value='Select models' />"
        return out

    def write_headers(self):
        self.send_response(200)
        self.send_header("Content-Type","text/html")
        self.send_header("Connnection","Close")
        self.end_headers()

    def do_GET(self):  
        self.write_headers()
        select = self.draw_manufacturers_select()
        self.wfile.write(b"<form method='post'>")
        self.wfile.write(select.encode("utf-8"))
        self.wfile.write(b"</form>")

    def do_POST(self):  
        l = int(self.headers["Content-Length"])
        data = self.rfile.read(l).decode("utf-8")
        params = parser.parse_qs(data)
        selectedModel = ""
        modselect = ""
        self.write_headers()
        if "mod_sel" in params:
            self.wfile.write(b"Selected models:<br>")
            for mod in params["models"]:
                self.wfile.write(f"{mod}<br>".encode("utf-8"))
            self.wfile.write(f"<hr>")

        if "manufacturer" in params:
            selectedModel = params["manufacturer"][0] 
        select = self.draw_manufacturers_select(selectedModel)
        if selectedModel:
            modselect = self.draw_model_select(selectedModel)
        self.wfile.write(b"<form method='post'>")
        self.wfile.write(select.encode("utf-8"))
        self.wfile.write(b"<br>")
        self.wfile.write(modselect.encode("utf-8"))
        self.wfile.write(b"</form>")
                
         


web_dir = os.path.join(os.path.dirname(__file__), '')
os.chdir(web_dir)

ss = server.HTTPServer(("0.0.0.0",8005),UploadHandler)
ss.serve_forever()



